-- # Sock merchant
-- https://www.hackerrank.com/challenges/sock-merchant

-- # Read input into list
readIntoList :: Read a => String -> [a]
readIntoList = (map read) . words
-- @ readIntoList "0 1 2" :: [Int]
-- * [0,1,2]

-- # Count occurences of a value in a list
countOccurences :: Int -> [Int] -> Int
countOccurences x lst = length (filter (\e -> e == x) lst)
-- @ countOccurences 1 [0,1,2,3,1,4]
-- * 2

-- # Remove occurences of a value in a list
removeOccurences :: Int -> [Int] -> [Int]
removeOccurences x lst = filter (\e -> e /= x) lst
-- @ removeOccurences 1 [0,1,2,3,1,4]
-- * [0,2,3,4]

-- # Calculate number of pairs of socks
sockMerchant :: [Int] -> Int
sockMerchant lst = accumulatePairsNumber 0 lst
    where
        accumulatePairsNumber :: Int -> [Int] -> Int
        accumulatePairsNumber n [] = n
        accumulatePairsNumber n (x:xs) = accumulatePairsNumber (n + div (countOccurences x (x:xs)) 2) (removeOccurences x xs)

solution :: String -> String
solution = show . sockMerchant . tail . (readIntoList :: String -> [Int])

main :: IO()
main = interact $ solution
--main = do print $ solution "9 10 20 20 10 10 30 50 10 20"