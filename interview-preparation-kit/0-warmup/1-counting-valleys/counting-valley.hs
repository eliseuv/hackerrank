-- # Counting valleys
-- https://www.hackerrank.com/challenges/counting-valleys

-- # Read input into list
readPath :: String -> String
readPath = concat . tail . words
-- @ readPath 8 UDDDUDUU
-- * "UDDDUDUU"

-- #
countValleys :: String -> Int
countValleys str = accumulateValleys 0 0 str
    where
        accumulateValleys :: Int -> Int -> String -> Int
        accumulateValleys valley_count _ [] = valley_count
        accumulateValleys valley_count level (x:xs) = accumulateValleys new_valley_count new_level (xs)
            where
                new_level = if x == 'D'
                            then level - 1
                            else if x == 'U'
                            then level + 1
                            else level
                new_valley_count =  if and [x == 'U',new_level == 0]
                                    then valley_count + 1
                                    else valley_count

solution :: String -> String
solution = show . countValleys . readPath

main :: IO()
main = interact $ solution
--main = do print $ (solution "8 UDDDUDUU")