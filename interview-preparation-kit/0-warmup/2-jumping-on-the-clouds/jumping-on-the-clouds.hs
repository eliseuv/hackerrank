-- # Jumping on the clouds
-- https://www.hackerrank.com/challenges/jumping-on-the-clouds

-- # Read input
readInput :: String -> [Bool]
readInput = (map (\i -> i == 1) :: [Int] -> [Bool]) . (filter (\i -> or [i == 0, i == 1])) . tail . (map read) . words
-- @ readInput "6 0 0 0 0 1 0"
-- * [False,False,False,False,True,False]

-- # Calculate jumps
calculateJumps :: [Bool] -> Int
calculateJumps = accumulate 0
    where
        accumulate :: Int -> [Bool] -> Int
        accumulate n_jumps [] = n_jumps
        accumulate n_jumps [_] = n_jumps
        accumulate n_jumps (_:b:xs) = accumulate (n_jumps+1) new_clouds
            where
                new_clouds = if or [xs == [],not (head xs)]
                                then xs
                                else (b:xs)

solution :: String -> String
solution = show . calculateJumps . readInput

main :: IO()
main = interact $ solution
--main = do print $ (solution "6 0 0 0 1 0 0")