-- # Repeated string
-- https://www.hackerrank.com/challenges/repeated-string

-- # Read input
readInput :: String -> (String,Int)
readInput str = (head $ str_lst, read $ head $ tail str_lst)
    where
        str_lst = words str

-- # Count the number of lower case a's on a given string
count_a's :: String -> Int
count_a's = length . (filter (\c -> c == 'a'))

-- # First n letters of infinitely repeating a given string
repeatString :: String -> Int -> String
repeatString str n = ((take n) . concat . repeat) $ str

repeatedString :: String -> Int -> Int
repeatedString str n = (n_whole * a_count) + a_count'
    where
        a_count = count_a's str
        a_count' = count_a's (take remaining_chars str)
        (n_whole, remaining_chars) = divMod n (length str)

-- # Elegant solution

elegantSolution :: String -> String
elegantSolution = show . count_a's . (uncurry $ repeatString) . readInput

-- # Performant solution

performantSolution :: String -> String
performantSolution = show . (uncurry $ repeatedString) . readInput

main :: IO()
main = interact $ performantSolution
--main = do print $ (performantSolution "aba 10")
