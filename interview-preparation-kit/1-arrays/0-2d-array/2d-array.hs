-- # 2D Array
-- https://www.hackerrank.com/challenges/2d-array

-- # Splits a list into chunks of fixed lenght
chunksOf :: Int -> [a] -> [[a]]
chunksOf 0 _ = []
chunksOf _ [] = []
chunksOf n lst = (fst splitted) : (chunksOf n (snd splitted))
    where
        splitted = splitAt n lst
-- @ chunksOf 2 [0,1,2,3,4,5]
-- * [[0,1],[2,3],[4,5]]

-- # Read matrix of given order from string to list of lists
readMatrix :: Int -> String -> [[Int]]
readMatrix n str = chunksOf n ((map read) $ words $ str)

-- # Calculates the "hourglass sum" centered at a given (i,j) for a given matrix
getHourglassSum :: [[Int]] -> (Int,Int) -> Int
getHourglassSum mat (i,j) = sum [   (mat!!(i-1)!!(j-1)) + (mat!!(i-1)!!(j)) + (mat!!(i-1)!!(j+1))
                                                        + (mat!!(i)!!(j))
                                  + (mat!!(i+1)!!(j-1)) + (mat!!(i+1)!!(j)) + (mat!!(i+1)!!(j+1))]

-- # Get all indices for all possible houglasses for a matrix of a given order
getIndices :: Int -> [(Int, Int)]
getIndices n = recursive (n-2) (n-2) []
    where
        recursive :: Int -> Int -> [(Int,Int)] -> [(Int,Int)]
        recursive 1 1 lst = ((1,1):lst)
        recursive i 1 lst = recursive (i-1) (n-2) ((i,1):lst)
        recursive i j lst = recursive i     (j-1) ((i,j):lst)

-- # Order of the matrix in the challenge
order :: Int
order = 6

solution :: String -> String
solution str = show $ maximum $ map (getHourglassSum matrix) (getIndices order)
    where
        matrix = readMatrix order str

main :: IO()
main = interact $ solution
--main = do print $ solution "1 1 1 0 0 0 0 1 0 0 0 0 1 1 1 0 0 0 0 0 2 4 4 0 0 0 0 2 0 0 0 0 1 2 4 0"