-- # Arrays: Left Rotation
-- https://www.hackerrank.com/challenges/ctci-array-left-rotation

readInput :: String -> (Int,[Int])
readInput str = (n_rot,arr)
    where
        str_lst = words str
        n_rot = read (str_lst!!1)
        arr = map read (drop 2 str_lst)

leftRotate :: Int -> [Int] -> [Int]
leftRotate n arr = (drop n arr) ++ (take n arr)

printArray :: [Int] -> String
printArray = unwords . (map show)

solution :: String -> String
solution = printArray . (uncurry leftRotate) . readInput

main :: IO()
main = interact $ solution
--main = do print $ solution "5 4 1 2 3 4 5"