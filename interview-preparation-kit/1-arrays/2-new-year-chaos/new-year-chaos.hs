-- # New Year Chaos
-- https://www.hackerrank.com/challenges/new-year-chaos

-- # Read Input string as a list of queues
readInput :: String -> [[Int]]
readInput str = loop input_lst []
    where
        input_lst = (map read) $ tail $ words $ str
        loop :: [Int] -> [[Int]] -> [[Int]]
        loop [] queue_lst = queue_lst
        loop (x:xs) queue_lst = loop (drop x xs) ((take x xs):queue_lst)

-- # Slice list
slice :: Int -> Int -> [a] -> [a]
slice _ _ [] = []
slice i j lst = if i <= j
                then (take (j-i+1)) $ (drop i) $ lst
                else []

-- # Calculates Minimum number of bribes for the queue. Returns Nothing if not possible
minBribes :: [Int] -> Maybe Int
minBribes queue = loop 0 (n-1)
    where
        n = length queue
        loop :: Int -> Int -> Maybe Int
        loop total_bribes (-1) = Just total_bribes
        loop total_bribes position =    if bribes_bought > 2
                                        then Nothing
                                        else loop (total_bribes+bribes_given) (position-1)
                                            where
                                                person = queue!!position
                                                original_position = person - 1
                                                bribes_bought = original_position - position
                                                bribes_given = length $ (filter (> person)) $ (slice (original_position-1) (position-1)) $ queue

-- # Prints result according to specifications
printSolution :: [Maybe Int] -> String
printSolution = concat . (map showMaybe) . reverse
    where
        showMaybe :: Maybe Int -> String
        showMaybe m_i = case m_i of
                            Just i  -> (show i)++"\n"
                            Nothing -> "Too chaotic\n"

solution :: String -> String
solution = printSolution . (map minBribes) . readInput
--solution = show . readInput

main :: IO()
main = interact $ solution
--main = do print $ solution "2 5 2 1 5 3 4 5 2 5 1 3 4"
--2 8 5 1 2 3 7 8 6 4 8 1 2 5 3 7 8 6 4