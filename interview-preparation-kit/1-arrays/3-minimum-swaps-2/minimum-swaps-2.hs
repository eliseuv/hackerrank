-- # Minimum Swaps 2
-- https://www.hackerrank.com/challenges/minimum-swaps-2

readInput :: String -> [Int]
readInput = (map (+(-1))) . (map read) . tail . words

--swap :: Int -> Int -> [Int] -> [Int]
--swap _ _ [] = []

swapElementsAt :: Int -> Int -> [a] -> [a]
swapElementsAt i j xs = let elemI = xs !! i
                            elemJ = xs !! j
                            left = take i xs
                            middle = take (j - i - 1) (drop (i + 1) xs)
                            right = drop (j + 1) xs
                        in  left ++ [elemJ] ++ middle ++ [elemI] ++ right

minimumSwaps :: [Int] -> Int
minimumSwaps = loop 0 0
    where
        loop :: Int -> Int -> [Int] -> Int
        loop n_swaps _ [] = n_swaps
        loop n_swaps pos (x:xs) =   if x == pos
                                    then loop n_swaps (pos+1) xs
                                    else loop (n_swaps+1) pos (swapElementsAt 0 (x-pos) (x:xs))

solution :: String -> String
solution = show . minimumSwaps . readInput

main :: IO()
--main = interact $ solution
main = do print $ solution "7 1 3 5 2 4 6 7"