-- # Solve me first
-- https://www.hackerrank.com/challenges/solve-me-first
-- Solution due to Tsoding (https://www.youtube.com/channel/UCEbYhDd6c6vngsF5PQpFVWg)

-- # Function application operator
-- - ($) :: (a -> b) -> a -> b
-- - f $ x = f x
-- - f1 (f2 (f3 x)) = f1 $ f2 $ f3 $ x

-- # Function composition operator
-- - (.) :: (b -> c) -> (a -> b) -> a -> c

-- # Separate string into list of words
-- - words :: String -> [String]
-- @ words "foo bar baz 0 1"
-- * ["foo","bar","baz", "0", "1"]

-- # Read string into data
-- - read :: Read a => String a -> a
-- @ read "0" :: Int
-- * 0

-- # Map
-- - map :: (a -> b) -> [a] -> [b]
-- @ map (2*) [0,1,2]
-- * [0,2,4]

-- # Read into list
readIntoList :: Read a => String -> [a]
readIntoList = (map read) . words
-- @ readIntoList "0 1 2" :: [Int]
-- * [0,1,2]

-- # Sum
-- - sum :: (Foldable t, Num a) => t a -> a
-- @ sum [0,1,2]
-- * 3
-- @ sum $ (map read) $ (words "0 1 2") :: Int
-- * 3

-- # Show data as String
-- - show :: Show a => a -> String
-- @ show 0
-- * "0"
-- @ show $ sum $ (map read) $ (words "0 1 2")
-- * "3"

-- # Sum numbers given as String
printSum :: String -> String
printSum = show . sum . readIntoList
-- @ printSum "0 1 2"
-- * "3"

-- # 
-- - interact :: (String -> String) -> IO ()
-- - interact :: (stdin -> stdout) -> side effect

main = interact $ printSum