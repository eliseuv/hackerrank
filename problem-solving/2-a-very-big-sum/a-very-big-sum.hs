-- # A very big sum
-- https://www.hackerrank.com/challenges/a-very-big-sum
-- Solution due to Tsoding (https://www.youtube.com/channel/UCEbYhDd6c6vngsF5PQpFVWg)

-- # Read into list rejecting the first element
readIntoList' :: Read a => String -> [a]
readIntoList' = (map read) . tail . words
-- @ readIntoList' "0 1 2" :: [Int]
-- * [1,2]

-- # Sum numbers given as String (rejecting the first element)
printSum' :: String -> String
printSum' = show . sum . readIntoList'
-- @ printSum "0 1 2"
-- * "3"

main = interact $ printSum'