-- # Compare triplets
-- https://www.hackerrank.com/challenges/compare-the-triplets

-- # Read input into list
readIntoList :: Read a => String -> [a]
readIntoList = (map read) . words

-- # Split at function
-- - splitAt :: Int -> [a] -> ([a], [a])
-- @ splitAt 3 [0,1,2,3,4,5]
-- * ([0,1,2],[3,4,5])

-- # Split into triplets
splitIntoTriplets :: [Int] -> ([Int], [Int])
splitIntoTriplets = splitAt 3

-- # Compare elementwise two lists in a pair
compareLists :: Ord a => (a -> a -> Bool) -> ([a],[a]) -> [Bool]
compareLists cmp = uncurry (zipWith cmp)
-- @ compareLists (>) ([8,2,9],[4,7,3])
-- * [True,False,True]

-- # Fold left
-- - foldl :: Foldable t => (b -> a -> b) -> b -> t a -> b

-- # Count number of Trues in a boolean list
countTrue :: [Bool] -> Int
countTrue = foldl (\n b -> if b then n+1 else n) 0
-- @ countTrue [True,False,True]
-- * 2

-- # List apply
-- Given a list of functions and a list of arguments, apply functions to arguments elementwise
listApply :: [a -> b] -> [a] -> [b]
listApply _ [] = []
listApply fs xs = ((head fs) $ (head xs)) : listApply (tail fs) (tail xs)
-- @ listApply [(+1),(+2)] [0,1]
-- * [1,3]

-- # Print list
printList :: Show a => [a] -> String
printList = unwords . (map show)
-- @ printList [0,1,2]
-- * "0 1 2"

-- Old solution
--solution :: String -> String
--solution = printList . (map countTrue) . (listApply [compareLists (>), compareLists (<)]) . (replicate 2 . splitIntoTriplets . readIntoList)

-- # Compose list of functions with another single function returning a list of composed functions
composeFuncs :: [b -> c] -> (a -> b) -> [a -> c]
composeFuncs [] _ = []
composeFuncs gs f = ((head gs) . f) : (composeFuncs (tail gs) f)
-- @ composeFuncs [g0,g1,g2] f
-- * [g0.f,g1.f,g2.f]

-- # Apply list of functions to a single input
applyFuncs :: [a -> b] -> a -> [b]
applyFuncs [] _ = []
applyFuncs fs x = ((head fs) $ x) : (applyFuncs (tail fs) x)
-- @ applyFuncs [f0,f1,f2] x
-- * [f0 x, f1 x, f2 x]

solution :: String -> String
solution = printList . (map countTrue) . applyFuncs (composeFuncs [compareLists (>), compareLists (<)] (splitIntoTriplets . readIntoList))

main :: IO()
main = interact $ solution
--main = do print $ solution "5 6 7 3 6 10"