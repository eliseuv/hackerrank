-- # Diagonal difference
-- https://www.hackerrank.com/challenges/diagonal-difference

-- # Read input into list
readIntoList :: Read a => String -> [a]
readIntoList = (map read) . words
-- @ readIntoList "0 1 2" :: [Int]
-- * [0,1,2]

-- # Splits a list into chunks of fixed lenght
chunksOf :: Int -> [a] -> [[a]]
chunksOf 0 _ = []
chunksOf _ [] = []
chunksOf n lst = (fst splitted) : (chunksOf n (snd splitted))
    where
        splitted = splitAt n lst
-- @ chunksOf 2 [0,1,2,3,4,5]
-- * [[0,1],[2,3],[4,5]]

-- # Build matrix from string where the first input is the order and the rest are the values
buildMatrix :: [Int] -> [[Int]]
buildMatrix lst = chunksOf order values
    where
        order = head lst
        values = tail lst
-- @ buildMatrix [3,11,2,4,4,5,6,10,8,-12]
-- * [[11,2,4],[4,5,6],[10,8,-12]]

-- # Get position of the last element of a list
lastElementPosition :: [a] -> Int
lastElementPosition lst = accumulate 0 lst
    where
        accumulate n [] = (n-1)
        accumulate n lst = accumulate (n+1) (tail lst)
-- @ lastElementPosition ['a','b','c']
-- * 2

-- # Diagonals of a square matrix
primaryDiagonal :: Num a => [[a]] -> [a]
primaryDiagonal mat = get 0 mat
    where
        get _ [] = []
        get n lst = (head lst)!!n : get (n+1) (tail lst)

secondaryDiagonal :: Num a => [[a]] -> [a]
secondaryDiagonal mat = get (lastElementPosition mat) mat
    where
        get _ [] = []
        get n lst = (head lst)!!n : get (n-1) (tail lst)

-- # Diagonal difference
diagonalDifference :: Num a => [[a]] -> a
diagonalDifference mat = abs (((sum . primaryDiagonal) mat) - ((sum . secondaryDiagonal) mat))

-- # Print list
printList :: Show a => [a] -> String
printList = unwords . (map show)
-- @ printList [0,1,2]
-- * "0 1 2"

solution :: String -> String
solution = show . diagonalDifference . buildMatrix . readIntoList

main ::IO()
main = interact $ solution
--main = do print $ solution "3 11 2 4 4 5 6 10 8 -12"